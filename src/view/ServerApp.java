package view;

import controller.ServerController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class ServerApp extends Application {
    private Stage primaryStage;
    private ServerController serverController;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;

        FXMLLoader mainLoader = new FXMLLoader(getClass().getResource("server.fxml"));
        Parent main = mainLoader.load();
        Scene mainScene = new Scene(main);
        serverController = mainLoader.getController();

        setUserAgentStylesheet(STYLESHEET_CASPIAN);

        primaryStage.setTitle("Prog 3 Mail Server");
        primaryStage.setScene(mainScene);
        primaryStage.show();
    }

    @Override
    public void stop() { // On stage exit
      serverController.onServerStop();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
