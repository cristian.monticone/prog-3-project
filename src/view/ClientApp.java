package view;

import controller.ClientController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class ClientApp extends Application {
    private Stage primaryStage;
    private ClientController clientController;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;

        FXMLLoader mainLoader = new FXMLLoader(getClass().getResource("client.fxml"));
        Parent main = mainLoader.load();
        Scene mainScene = new Scene(main);
        clientController = mainLoader.getController();

        setUserAgentStylesheet(STYLESHEET_CASPIAN);

        primaryStage.setTitle("default@prog3mail.com");
        primaryStage.setScene(mainScene);
        primaryStage.show();
    }

    @Override
    public void stop() { // On stage exit
      clientController.onClose();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
