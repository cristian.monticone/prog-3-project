#!/bin/sh

BASE_DIR="$(git rev-parse --show-toplevel)"

RIGHTSHA256_JFX="8de2c84a5844341d140074f5070deca1f7865733ef0176a8114540a9db2e4657  openjfx-12.0.1_linux-x64_bin-sdk.zip"

wget "https://download2.gluonhq.com/openjfx/12.0.1/openjfx-12.0.1_linux-x64_bin-sdk.zip"

FILENAME_JFX="openjfx-12.0.1_linux-x64_bin-sdk.zip"

REMOTESHA256_JFX=$(sha256sum $FILENAME_JFX)

if [ "$REMOTESHA256_JFX" != "$RIGHTSHA256_JFX" ]; then
  echo "$FILENAME_JFX checksum failed!" >&2
  exit 1
fi

unzip $FILENAME_JFX > /dev/null

mkdir libs 2> /dev/null

mv javafx-sdk-12.0.1/lib/*.jar libs/

rm -rf javafx-sdk-12.0.1/

rm $FILENAME_JFX

echo "Setup completed."
