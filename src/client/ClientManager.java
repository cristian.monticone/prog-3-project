package client;

import protocol.ProtocolClient;
import protocol.ProtocolException;
import data.User;
import data.Config;
import data.Mail;

import java.net.Socket;
import java.net.ConnectException;
import java.util.List;
import java.util.ArrayList;

public class ClientManager {
  private User currentUser;
  private Long lastMail;

  public ClientManager() {
    currentUser = new User("default@prog3mail.com");
    lastMail = null;
  }

  public void changeUser(User newUser) {
    currentUser = newUser;
  }

  public String getAddressName() {
    return currentUser.toString();
  }

  public void sendMail(List<User> destList, String subject, String content) 
                                    throws ConnectException {
    ProtocolClient protocol = startConnection();
    Mail mail = new Mail(new ArrayList<User>(destList), subject, content);

    try {
      protocol.sendMessageRequest(mail);
      protocol.closeConnection();
    }
    catch (ProtocolException pe) { throw pe; }
    catch (ConnectException ce) { throw ce; }
    catch (Exception e) { e.printStackTrace(); }
  }

  public List<Mail> synchMails() throws ConnectException {
    ProtocolClient protocol = startConnection();
    List<Mail> mailList = null;

    try {
      mailList = protocol.synchRequest(lastMail);
      protocol.closeConnection();
    }
    catch (ProtocolException pe) { throw pe; }
    catch (ConnectException ce) { throw ce; }
    catch (Exception e) { e.printStackTrace(); }

    updateLastMail(mailList);

    return mailList;
  }

  public void removeMail(Mail mail) throws ConnectException {
    ProtocolClient protocol = startConnection();

    try {
      protocol.sendRemoveMessageRequest(mail.getId());
      protocol.closeConnection();
    }
    catch (ConnectException ce) { throw ce; }
    catch (Exception e) { e.printStackTrace(); }
  }

  private ProtocolClient startConnection() throws ConnectException {
    ProtocolClient protocol = null;
    try {
      Socket socket = new Socket(Config.DEFAULT_LISTENING_ADDRESS,
                                 Config.DEFAULT_PORT);
      socket.setSoTimeout(2000); // 2 seconds of socket timeout.

      protocol = new ProtocolClient(socket);
      
      protocol.init(currentUser);
    }
    catch (ConnectException ce) { throw ce; }
    catch (ProtocolException pe) { throw pe; }
    catch (Exception e) { e.printStackTrace(); }

    return protocol;
  }

  public void resetLastMail() {
    lastMail = null;
  }

  private void updateLastMail(List<Mail> mailList) {
    if (!mailList.isEmpty()) {
      Long max = Long.MIN_VALUE;
      
      for (Mail mail: mailList)
        max = ( max.compareTo(mail.getId()) < 0 ) ? mail.getId() : max;

      lastMail = max;
    }
  }
}
