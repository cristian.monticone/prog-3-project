package client;

import protocol.ProtocolClient;
import java.net.Socket;

import data.User;

import java.util.ArrayList;

public class CommandClient {
  public static void main(String args[]) {
    System.out.println("Warning! This is a debug purpose client.");

    if (args.length < 2) usage();
    else {
      // Example
      try {
        Socket s = new Socket("localhost", 1234);
        s.setSoTimeout(2000); // 2 seconds of socket timeout.

        ProtocolClient c = new ProtocolClient(s);

        User u = new User(args[1]);
        c.init(u);

        if (args[0].equals("synch")) 
          c.synchRequest();
        else if (args[0].equals("remove"))
          c.sendRemoveMessageRequest(Long.parseLong(args[2]));
        else if (args[0].equals("send")) {
          ArrayList<User> dest = new ArrayList<>();
          dest.add(new User(args[2]));
          data.Mail m = new data.Mail(null, u, dest, args[4], args[3], null);
          c.sendMessageRequest(m);
        }
        else
          usage();

        s.close();
      }
      catch (Exception e) { e.printStackTrace(); }
    }
  }

  public static void usage() { 
    System.out.println("Usage: java client.Client <synch|remove|send> <address> [...]");
  }
}
