package data;

public interface LoggerReceiver {
  public void logEvent(String msg);
}
