/**
 * @author Scarso Giordano
 * @version %I%
 */


package data;

import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;

public class BaseId implements Serializable {
  private long id;
  private String path;

  public BaseId(String basePath) {
    id = 0;
    path = basePath;
  }

  public synchronized long newId() {
    loadId();
    id++;
    writeId();
    return id;
  }

  public synchronized void resetId() {
    id = 0;
    writeId();
  }

  private void loadId() {
    try { 
      BaseId b = (BaseId) IOManager.load(path+"baseId.bin");
      System.out.println(b);
      id = b.id;
      path = b.path;
    }
    catch(FileNotFoundException e) {
      resetId();
    }
    catch(Exception e) {
      System.out.format("%s%n",e);
    }
  }

  private void writeId() {
    IOManager.write(path,"baseId.bin",this);
  }

  public String toString() {
    return "BaseId id:"+id+" Path:"+path;
  }
}
