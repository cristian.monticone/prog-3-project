package data;

import java.util.ArrayList;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Collection;
import java.io.FileNotFoundException;

public class ClientDataModel {
  private Config config;
  private User user;
  private ArrayList<Mail> mailBox;
  private final static String CONFIG_PATH = "config.bin";

  public ClientDataModel(String address) {
    config = new Config();
    user = new User(address);
    mailBox = new ArrayList<Mail>();
  }

  //Getters
  public Config getConfig() { return config; }
  public User getUser() { return user; }
  public ArrayList<Mail> getMailBox() { return mailBox; }

  /**
   * Supporre config istanziato, caricamento user.bin,
   * caricare le mailbox nell'hashmap. 
   */
  public void loadData() {
    config.loadConfig();
    readUser();
    readMail();
  }

  /**
   * Aggiunge la mail data.
   * @param mail La Mail da aggiungere.
   */
  public void addMail(Mail mail){
      mailBox.add(mail);
  }

  /**
   * Rimuove la mail con identificativo uguale ad id.
   * @param id    Identificativo della mail che vogliamo rimuovere.
   */
  public void removeMail(User user, Long id) {
        mailBox.removeIf(m -> (m.equals(id)));
        writeMail();
  }

  /**
   * Scrittura su file degli utenti registrati.
   * @param user Elenco degli utenti da memorizzare
   */
  public void writeUser() {
    if(user != null)
      IOManager.write(config.getBaseFolder(),user.getAddress()+"_client.bin",user);
  }

  public void readUser() {
      try {
        user = (User)IOManager.load(
            config.getBaseFolder()+user.getAddress()+"_client.bin");
      }
      catch (Exception e) {
        System.out.format("%s%n",e);
      }
  }

  /**
   * Scrittura su file della mailBox dell'utente.
   * @param user Utente del quale memorizziamo la mailBox
   */
  public void writeMail() {
    IOManager.write(config.getBaseFolder(),user.getAddress()+"_mail.bin",mailBox);
  }

  /**
   * Carica le mail dai file.
   */
  @SuppressWarnings("unchecked")
  public void readMail() {
      try {
        mailBox = (ArrayList<Mail>) 
          IOManager.load(config.getBaseFolder()+user.getAddress()+"_mail.bin");
      }
      catch(Exception e) {
        System.out.format("%s%n",e);
      }
  }

  public String toString() {
    return user+" "+config+" "+mailBox;
  }
}
