/**
 * @author Scarso Giordano
 * @version %I%
 */
package data;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.File;
import java.io.Serializable;
import java.io.IOException;
import java.io.FileNotFoundException;

public class Config implements Serializable{
  public static int DEFAULT_PORT = 1234;
  public static String DEFAULT_LISTENING_ADDRESS = "localhost";
  public static String DEFAULT_LOG_PATH = "base/log";
  public static String DEFAULT_BASE_FOLDER = "base/";
  private int port;
  private String listeningAddress;
  private String logPath;
  private String baseFolder;
  private LogLevel logLevel;

  public Config() {
    loadDefault();
  }
  
  public Config(int port, String listeningAddress,
      String logPath, String baseFolder) {
    this.port = port;
    this.listeningAddress = listeningAddress;
    this.logPath = logPath;
    this.baseFolder = baseFolder;
  }

  // Getters
  public int getPort() { return port; }
  public String getListeningAddress() { return listeningAddress; }
  public String getLogPath() { return logPath; }
  public String getBaseFolder() { return baseFolder; }
  public LogLevel getLogLevel() { return logLevel; }

  //Setter
  public void setPort(int port) { this.port = port; }
  public void setListeningAddress(String listeningAddress) { 
    this.listeningAddress = listeningAddress; 
  }
  public void setLogPath(String logPath) { this.logPath = logPath; }
  public void setBaseFolder(String baseFolder) {
    this.baseFolder = baseFolder; 
  }
  public void setLogLevel(LogLevel logLevel) {
    this.logLevel = logLevel;
  }

  public void loadConfig() {
    try { 
      Config c = (Config) IOManager.load(
          baseFolder.toString()+"config.bin");
      port = c.port;
      listeningAddress = c.listeningAddress;
      logPath = c.logPath;
      logLevel = c.logLevel;
      baseFolder = c.baseFolder;
    }
    catch(FileNotFoundException e) {
      loadDefault();
      writeConfig();
    }
    catch(Exception e) {
      System.out.format("%s%n",e);
    }
  }

  public void writeConfig() {
    IOManager.write(baseFolder,"/config.bin",this);
  }

  public void loadDefault() {
    port = DEFAULT_PORT;
    listeningAddress = DEFAULT_LISTENING_ADDRESS;
    logPath = DEFAULT_LOG_PATH;
    baseFolder = DEFAULT_BASE_FOLDER;
    logLevel = LogLevel.DEFAULT;
  }

  public String toString() {
    return "port:" + port +
      " listeningAddress:" + listeningAddress +
     " logPath:" + logPath + " baseFolder:" + baseFolder;
  } 
}
