package data;

import java.util.Date;
import java.util.ArrayList;
import java.io.Serializable;

public class Mail implements Serializable {
  private Long id;
  private String text;
  private String subject;
  private Date timestamp;
  private User sender;
  private ArrayList<User> destList;

  public Mail(Long id, User sender, ArrayList<User> destList, String text, String subject, Date timestamp) {
    setId(id);
    setText(text);
    setSubject(subject);
    setTimestamp(timestamp);
    setSender(sender);
    setDestList(destList);
  }

  public Mail(ArrayList<User> destList, String subject, String text) {
    this(null, null, destList, text, subject, null);
  }

  public Mail(Long id) {
    setId(id);
  }

  /* Getters */
  public Long getId() { return id; }

  public String getText() { return text; }

  public String getSubject() { return subject; }

  public Date getTimestamp() { return timestamp; }

  public User getSender() { return sender; }

  public ArrayList<User> getDestList() { return new ArrayList<>(destList); }

  /* Setters */
  public void setId(Long id) { this.id = id; }

  public void setText(String text) { this.text = text; }

  public void setSubject(String subject) { this.subject = subject; }

  public void setTimestamp(Date timestamp) { this.timestamp = timestamp; }

  public void setSender(User sender) { this.sender = sender; }

  public void setDestList(ArrayList<User> destList) { this.destList = destList; }

  // TODO improve this
  public String toString() { 
    return "\nId: "+ id + "\nFrom: " + sender + ";\nTo: " + destList + 
      ";\nSubject: " + subject + ";\nText: " + text + ";\nDate: " + timestamp +
      ";\n"; }

  @Override
  public int hashCode() {
    return id.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) return false;
    Class objClass = obj.getClass();

    // A Long object passed: checking if equals with this mail Long id.
    if (objClass == Long.class) {
      return id.equals(obj);
    }
    // A Mail object passed: checking if the long id inside obj equals this 
    // mail id.
    else if(objClass == Mail.class) {
      Mail objMail = (Mail) obj;
      return id.equals(objMail.id);
    }
    // obj isn't a Long and neither a Mail, return false.
    else {
      return false;
    }
  }
}
