package data;

import java.util.HashMap;
import java.util.HashSet;
import java.util.ArrayList;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Collection;
import java.io.FileNotFoundException;

public class DataModel {
  private Config config;
  private HashSet<User> users;
  private HashMap<User, ArrayList<Mail>> mailBoxes;
  private final static String CONFIG_PATH = "config.bin";
  private BaseId id;
  private Logger logger;

  public DataModel() {
    config = new Config();
    users = new HashSet<User>();
    mailBoxes = new HashMap<User, ArrayList<Mail>>();
    id = new BaseId(config.getBaseFolder());
    logger = new Logger(LogLevel.DEFAULT);
  }

  //Getters
  public Config getConfig() { return config; }
  public HashSet<User> getUsers() { return users; }
  public HashMap<User, ArrayList<Mail>> getMailBoxes() { return mailBoxes; }
  public BaseId getId() { return id; }
  public Logger getLogger() { return logger; }

  /**
   * Supporre config istanziato, caricamento users.bin,
   * caricare le mailbox nell'hashmap. 
   */
  public void loadData() {
    config.loadConfig();
    logger.changeLogLevel(config.getLogLevel());
    readUsers();
    readMailBox();
  }
  
  /**
   * Aggiunge la mail data alle mailBox di tutti gli utenti presenti nella sua
   * lista di destinatari ed in quella del mittente assegnando gli id corretti.
   * @param mail La mail che verra aggiunta alle mailBox.
   */
  public void sendMail(Mail mail) {
    ArrayList<User> destList = mail.getDestList();
    User sender = mail.getSender();

    mail.setId(id.newId());

    // Controllo per evitare di aggiungere due volte la mail nella mailbox
    // del mittente quando lui stesso è tra i destinatari.
    /*if (!destList.contains(sender)) { 
      try { addMail(mail, sender); }
      catch (UserNotFoundException e) { System.out.println(e); return; }
    } */

    for(User dest: destList) {
      try { addMail(mail, dest); }
      catch (Exception e) { e.printStackTrace(); }
    }
  }

  /**
   * Aggiunge la mail data alla mailBox dell'utente scelto.
   * @param mail La Mail da aggiungere.
   * @param user L'utente proprietario della mailBox.
   */
  public void addMail(Mail mail, User user) throws UserNotFoundException {
    if(!userExists(user)) {
      throw new UserNotFoundException(user);
    }
    synchronized (mailBoxes.get(user)) {
      IOManager.write(config.getBaseFolder(), user.getAddress()+".bin", mail, !mailBoxes.get(user).isEmpty());
      mailBoxes.get(user).add(mail);
    }
  }

  /**
   * Rimuove la mail con identificativo uguale ad id del dato utente.
   * @param user  Utente proprietario della mailbox dalla quale vogliamo 
   * cancellare una mail.
   * @param id    Identificativo della mail che vogliamo rimuovere.
   */
  public void removeMail(User user, Long id) {
    if(userExists(user)) {
      synchronized (mailBoxes.get(user)) {
        System.out.println(id);
        mailBoxes.get(user).removeIf(m -> (m.equals(id)));

        boolean append = false;
        if (mailBoxes.get(user).isEmpty())
          new File(config.getBaseFolder()+user.getAddress()+".bin").delete();
        else
          for (Mail mail: mailBoxes.get(user)) {
            IOManager.write(config.getBaseFolder(), user.getAddress()+".bin", mail, append);
            append = true;
          }
      }
    }
  }

  /**
   * Crea una nuova mailBox per l'utente dato.
   * @param user L'utente per cui si crea la mailBox.
   */
  public void createMailBox(User user) {
    if(!userExists(user))
      mailBoxes.put(user,new ArrayList<Mail>());
  }

  public void addUser(User user) {
    if(user != null) {
      users.add(user);
      createMailBox(user);
      writeUsers();
    }
  }

  public void removeUser(User user) {
    if(user != null)
      mailBoxes.remove(user, mailBoxes.get(user));
      users.remove(user);
  }
  /**
   * Scrittura su file degli utenti registrati.
   * @param users Elenco degli utenti da memorizzare
   */
  public void writeUsers() {
    if(users != null)
      IOManager.write(config.getBaseFolder(),"users.bin",users);
  }

  @SuppressWarnings("unchecked")
  public void readUsers() {
      try {
        users = (HashSet<User>)  IOManager.load(
            config.getBaseFolder()+"users.bin");
      }
      catch (FileNotFoundException e) {
        addUser(new User("default@prog3mail.com"));
        addUser(new User("cristian@prog3mail.com"));
        addUser(new User("giordano@prog3mail.com"));
        writeUsers();
      }
      catch (Exception e) {
        e.printStackTrace();
      }
  }

  /**
   * Carica le mail box dai file.
   */
  @SuppressWarnings("unchecked")
  public void readMailBox() {
    ArrayList<Mail> list;
    for(User u: users) {
      try {
        list = (ArrayList<Mail>) 
          IOManager.loadList(config.getBaseFolder()+u.getAddress()+".bin");
        mailBoxes.put(u,list);
      }
      catch(FileNotFoundException e) {
        list = new ArrayList<Mail>();
        mailBoxes.put(u,list);
      }
      catch(Exception e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * Restituisce la collezione di Mail dell'utente ricevute con id maggiore di lastId,
   * se lastId non risulta presente nella mailBox dell'utente viene restituita
   * l'intera lista di mail ricevute.
   * @param user    Utente del quale si desidera ricevere la posta.
   * @param lastId  L'id dell'ultima mail ricevuta dall'utente.
   * @return La collezione di mail ricevute dall'utente, null se questo non
   * esiste.
   */
  public ArrayList<Mail> getMails(User user, Long lastId) {
    if(userExists(user)) {
      ArrayList<Mail> tmp = mailBoxes.get(user);

      // With a null as lastId the full user's mail list had to be returned.
      if (lastId == null)
        return tmp;

      //int tmpIndex = tmp.lastIndexOf(new Mail(lastId));

      int tmpIndex = tmp.size();
      for (Mail mail: tmp) { // TODO refactor this
        if (mail.getId() >= lastId) {
          tmpIndex = tmp.indexOf(mail);
          if (lastId.equals(mail.getId()))
            tmpIndex++;
          break;
        }
      }

      //if (tmpIndex == -1)
      //  return tmp;

      return new ArrayList<>(tmp.subList(tmpIndex, tmp.size()));
    }
    return null;
  }

  /**
   * Verifica l'esistenza del dato utente.
   * @param user L'utente di cui voglio verificare l'esistenza.
   * @return true se l'utente esiste, falso altrimenti.
   */
  public boolean userExists(User user) {
    if(user == null)
      return false;
    return mailBoxes.containsKey(user);
  }

  public String toString() {
    return ""+config+id+mailBoxes;
  }
}
