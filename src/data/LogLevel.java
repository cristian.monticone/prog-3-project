package data;

import java.lang.Comparable;

import java.util.List;
import java.util.ArrayList;
import java.io.Serializable;

public class LogLevel implements Serializable, Comparable<LogLevel> {
  public static final LogLevel ALL    = new LogLevel(0, "ALL");
  public static final LogLevel DEBUG  = new LogLevel(1, "DEBUG");
  public static final LogLevel CONN   = new LogLevel(2, "CONN");
  public static final LogLevel INFO   = new LogLevel(3, "INFO");
  public static final LogLevel WARN   = new LogLevel(4, "WARN");
  public static final LogLevel ERROR  = new LogLevel(5, "ERROR");
  public static final LogLevel FATAL  = new LogLevel(6, "FATAL");
  public static final LogLevel SYSTEM = new LogLevel(7, "SYSTEM");

  public static final LogLevel DEFAULT = INFO;

  private int level;
  private String label;

  private LogLevel(int level, String label) {
    this.level = level;
    this.label = label;
  }

  public int compareTo(LogLevel logLevel) {
    return Integer.compare(level, logLevel.level);
  }

  public static List<LogLevel> getAllLevels() {
    ArrayList<LogLevel> levels = new ArrayList<>();

    levels.add(ALL);
    levels.add(DEBUG);
    levels.add(CONN);
    levels.add(INFO);
    levels.add(WARN);
    levels.add(ERROR);
    levels.add(FATAL);
    levels.add(SYSTEM);

    return levels;
  }

  @Override
  public String toString() {
    return label;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) return false;
    Class objClass = obj.getClass();

    if (objClass == LogLevel.class) {
      return level == ((LogLevel)obj).level;
    }
    else if(objClass == Integer.class) {
      return level == ((Integer)obj);
    }
    else {
      return false;
    }
  }
}
