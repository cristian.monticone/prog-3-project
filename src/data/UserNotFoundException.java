package data;

public class UserNotFoundException extends Exception {
  public UserNotFoundException(User u) {
      super(u.toString()+" non trovato");
  }
}
