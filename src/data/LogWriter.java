package data;

import java.io.File;
import java.io.PrintWriter;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.lang.SecurityException;

public class LogWriter {
  private PrintWriter output;
  private Config config;

  public LogWriter(String filename, Logger logger, Config config) {
    try {
      output = new PrintWriter(new FileOutputStream(new File(filename), true));
      this.config = config;
      logger.addReceiver( (String msg) -> {
        output.println(msg);
        output.flush();
      });
    }
    catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    catch (SecurityException e) {
      e.printStackTrace();
    }
  }

  public void close() {
    output.flush();
    output.close();
  }

  public void updatePath() {
    output.close();
    try {
      output = new PrintWriter(
          new FileOutputStream(new File(config.getLogPath()), true));
    }
    catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    catch (SecurityException e) {
      e.printStackTrace();
    }
  }

}
