package data;

import java.util.ArrayList;

public class Logger {
  ArrayList<LoggerReceiver> receivers;
  private LogLevel logLevel;

  public Logger(LogLevel logLevel) {
    receivers = new ArrayList<>();
    this.logLevel = logLevel;
  }

  public Logger() {
    this(LogLevel.DEFAULT);
  }

  public void changeLogLevel(LogLevel newLevel) {
    logLevel = newLevel;
  }

  public void log(LogLevel logLevel, String msg) {
    if (logLevel.compareTo(this.logLevel) >= 0) {
      String caller = new Exception().getStackTrace()[1].getClassName();
      msg = "[" + logLevel + "][" + caller + "] " + msg;

      for(LoggerReceiver rec: receivers) {
        rec.logEvent(msg);
      }
    }
  }

  public void addReceiver(LoggerReceiver rec) {
    receivers.add(rec);
  }

  public void removeReceiver(LoggerReceiver rec) {
    receivers.remove(rec);
  }
}
