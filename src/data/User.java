package data;

import java.io.Serializable;

public class User implements Serializable {
  private String address;

  public User(String address) {
    setAddress(address);
  }

  /* Getters */
  public String getAddress() { return address; }

  /* Setters */
  public void setAddress(String address) { this.address = address; }

  public String toString() {
    return address;
  }

  @Override
  public int hashCode() {
    return address.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) return false;
    Class objClass = obj.getClass();

    // A String object passed: checking if equals with this user String 
    // address.
    if (objClass == String.class) {
      return address.equals(obj);
    }
    // A User object passed: checking if the String address inside obj equals
    // with this user address.
    else if(objClass == User.class) {
      User objUser = (User) obj;
      return address.equals(objUser.address);
    }
    // obj isn't a String and neither a User, return false.
    else {
      return false;
    }
  }
}
