package data;

import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.ClassNotFoundException;
import java.io.File;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.io.EOFException;

public class IOManager {
  public static<T> Object load(String path)
    throws IOException, ClassNotFoundException{
    try (ObjectInputStream in = new ObjectInputStream( 
          new FileInputStream(path))
        ){
        return in.readObject();
        }
  }

  public static<T> Object loadList(String path)
    throws IOException, ClassNotFoundException{
    try (ObjectInputStream in = new ObjectInputStream( 
          new FileInputStream(path))
        ){

        boolean eof = false;
        ArrayList<Object> objList = new ArrayList<>();
        while (!eof) {
          try {
            Object obj = in.readObject();
            objList.add(obj);
          }
          catch (EOFException e) {
            eof = true;
          }
        }

        return objList;
    }
  }

  public static<T> void write(String path, T source) {
    write(path, "", source, false);
  }

  public static<T> void write(String path, T source, boolean append) {
    write(path, "", source, append);
  }

  public static<T> void write(String folder, String name, T source) {
    write(folder, name, source, false);
  }

  public static<T> void write(String folder, String name, T source, boolean append) {
    File f = new File(folder);
    f.mkdir();
    try {new File(f,name).createNewFile(); }
    catch (Exception e) { System.out.format("%s%n",e); }
    writeOnFile(folder + name, source, append);
  }

  private static<T> void writeOnFile(String path, T source, boolean append) {
    try {
      ObjectOutputStream out;
      OutputStream outStream = new FileOutputStream(path, append);

      if (append)
        out = new AppendingObjectOutputStream(outStream);
      else
        out = new ObjectOutputStream(outStream);

      out.writeObject(source);
    }
    catch(Exception e) {
      System.out.format("%s%n",e);
    }
  }
}
