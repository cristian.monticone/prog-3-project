package data;

import java.io.Serializable;

public class Error implements Serializable {
  public static final Error BAD_REQUEST     = new Error(400, "BAD_REQUEST");
  public static final Error DEST_NOT_FOUND  = new Error(404, "DEST_NOT_FOUND");
  public static final Error INVALID_DATA    = new Error(422, "INVALID_DATA");
  public static final Error INTERNAL        = new Error(500, "INTERNAL");
  public static final Error NOT_IMPLEMENTED = new Error(501, "NOT_IMPLEMENTED");
  public static final Error UNKNOWN         = new Error(520, "UNKNOWN");

  private int errorCode;
  private String errorName;

  private Error(int errorCode, String errorName) {
    this.errorCode = errorCode;
    this.errorName = errorName;
  }

  public int getCode() {
    return errorCode;
  }

  public String getName() {
    return errorName;
  }

  @Override
  public String toString() {
    return errorName;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) return false;
    Class objClass = obj.getClass();

    if (objClass == Error.class) {
      return errorCode == ((Error)obj).errorCode;
    }
    else if(objClass == Integer.class) {
      return errorCode == ((Integer)obj);
    }
    else {
      return false;
    }
  }
}
