package server;

import protocol.ProtocolServer;
import protocol.ProtocolException;
import data.DataModel;
import data.Logger;
import data.LogLevel;

import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

public class SocketHandler implements Runnable {
  private Socket s;
  private ProtocolServer protocolHandler;
  private DataModel model;
  private Logger logger;

  public SocketHandler(Socket s, DataModel model) {
    if (s == null) 
      throw new NullPointerException("Trying to instance a SocketHandler without a socket.");

    if (model == null) 
      throw new NullPointerException("Trying to instance a SocketHandler without a data model.");

    this.s = s;
    this.model = model;
    this.logger = model.getLogger();
  }

  public void run() {
    try {
      try {
        protocolHandler = new ProtocolServer(s, model);
        protocolHandler.handle();
      }
      catch (ProtocolException pe) {
        logger.log(LogLevel.ERROR, "Connection stopped by a ProtocolException.");
        logger.log(LogLevel.DEBUG, "ProtocolException: " + pe);
      }
      catch (SocketException se) {
        logger.log(LogLevel.ERROR, "Connection stopped by a SocketException.");
        logger.log(LogLevel.DEBUG, "SocketException: " + se);
      }
      catch (SocketTimeoutException ste) {
        logger.log(LogLevel.WARN, "Connection has gone in timeout.");
        logger.log(LogLevel.DEBUG, "SocketTimeoutException: " + ste);
      }
      finally {
        s.close();
      }
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }
}
