package server;

import data.DataModel;

import java.net.ServerSocket;
import java.net.Socket;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

import java.util.concurrent.TimeUnit;
import java.net.InetSocketAddress;
import java.net.SocketException;

import data.Logger;
import data.LogLevel;

public class Dispatcher implements Runnable {
  // Max time in milliseconds until a connection goes in timeout.
  public final static int CONNECTION_TIMEOUT = 2000;
  // Max thread per time.
  public final static int NUM_THREAD = 10;
  // Max time in seconds for the shutdown procedure.
  public final static int AWAIT_TIME = 60;

  private ExecutorService pool;
  private DataModel model;
  private volatile boolean shutdown;
  private Logger logger;
  private volatile ServerSocket welcomeSocket;

  public Dispatcher(DataModel model) { 
    pool = Executors.newFixedThreadPool(NUM_THREAD);
    this.model = model;
    logger = model.getLogger();
    welcomeSocket = null;
  }

  public void run() {
    shutdown = false;

    try {
      int port = model.getConfig().getPort();
      welcomeSocket = new ServerSocket();
      welcomeSocket.setReuseAddress(true);
      welcomeSocket.bind(new InetSocketAddress(port));

      logger.log(LogLevel.SYSTEM, "Server successfully started.");

      while (!shutdown) {
        Socket incoming = welcomeSocket.accept();
        incoming.setSoTimeout(CONNECTION_TIMEOUT);
        Runnable r = new SocketHandler(incoming, model);

        pool.execute(r);
      }
    }
    catch (SocketException se) {
      logger.log(LogLevel.DEBUG, "Dispacher stopped.");
    }
    catch (IOException e) {
      logger.log(LogLevel.FATAL, 
                 "An IOException occurred dispatching connections.");
      logger.log(LogLevel.DEBUG, "IOException: " + e);
      e.printStackTrace();
    }
    finally {
      try {
        welcomeSocket.close();
      }
      catch (IOException e) {
      logger.log(LogLevel.FATAL, 
                 "An IOException occurred closing welcome socket.");
      logger.log(LogLevel.DEBUG, "IOException: " + e);
        e.printStackTrace();
      }
    }

    logger.log(LogLevel.SYSTEM, "Server shutdown...");

    pool.shutdown();
    try {
      if (!pool.awaitTermination(AWAIT_TIME, TimeUnit.SECONDS)) {
          logger.log(LogLevel.WARN,
                     "Time exceeded during soft dispatcher termination, " +
                     "forcing shutdown...");
          pool.shutdownNow();
      }
    } catch (InterruptedException ex) {
      logger.log(LogLevel.WARN,
                 "Dispatcher interrupted during soft shutdown procedure, " +
                 "forcing shutdown...");
      pool.shutdownNow();
      Thread.currentThread().interrupt();
    }

    logger.log(LogLevel.SYSTEM, "Server successfully stopped.");
  }

  public void shutdown() {
    shutdown = true;
    try {
      welcomeSocket.close();
    }
    catch (IOException e) {
      logger.log(LogLevel.FATAL, 
                 "An IOException occurred during shutdown procedure.");
      logger.log(LogLevel.DEBUG, "IOException: " + e);
      e.printStackTrace();
    }
  }
}
