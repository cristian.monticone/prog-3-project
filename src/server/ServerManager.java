package server;

import data.LogWriter;
import data.DataModel;
import data.Logger;
import data.LogLevel;

public class ServerManager {
  private DataModel model;
  private Dispatcher dispatcher;
  private boolean running;
  private Logger logger;
  private LogWriter logWriter;
    
  public ServerManager() {
    model = new DataModel();
    model.loadData();
    logger = model.getLogger();
    logWriter = new LogWriter(model.getConfig().getLogPath(), logger, 
        model.getConfig());
    running = false;
  }

  public void start() {
    if (running) {
      logger.log(LogLevel.ERROR, "Server already running!");
      return;
    }

    logWriter.updatePath();
    dispatcher = new Dispatcher(model);
    new Thread(dispatcher).start();

    running = true;
  }

  public void stop() {
    if (!running) {
      logger.log(LogLevel.ERROR, "Server already stopped!");
      return;
    }

    dispatcher.shutdown();

    logWriter.close();

    running = false;
  }

  public DataModel getModel() {
    return model;
  }

  public LogWriter getLogWriter() {
    return logWriter;
  }
}
