#!/bin/sh

BUILD_DIR="build"

cd $BUILD_DIR

java --module-path ../libs --add-modules javafx.fxml,javafx.controls view.ClientApp
