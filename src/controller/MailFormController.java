package controller;

import javafx.fxml.FXML;

import javafx.scene.control.TextInputDialog;
import javafx.scene.control.TextField;
import javafx.scene.control.TextArea;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.application.Platform;

import data.Mail;
import data.User;
import data.Error;

import protocol.ProtocolException;
import protocol.ErrorToken;

import client.ClientManager;

import java.util.ArrayList;
import java.util.Optional;
import java.net.ConnectException;

public class MailFormController {
  private ClientManager clientManager;
  private Mail openedMail;
  private boolean onReply = false;
  
  @FXML
  private TextField destField;

  @FXML
  private TextField subjectField;

  @FXML
  private TextArea messageArea;

  @FXML
  private Button cancelButton;

  @FXML
  private Button sendButton;

  @FXML
  private Button replyButton;

  @FXML
  private Button replyAllButton;

  @FXML
  private Button forwardButton;

  @FXML
  private Label destLabel;

  @FXML
  public void initialize() {
  
  }

  public void setClientManager(ClientManager clientManager) {
    this.clientManager = clientManager;
  }

  public void openMail(Mail mail) {
    sendButton.setVisible(false);
    replyAllButton.setVisible(true);
    replyButton.setVisible(true);
    forwardButton.setVisible(true);

    destLabel.setText("From:");

    messageArea.setEditable(false);
    subjectField.setEditable(false);
    destField.setEditable(false);

    subjectField.setText(mail.getSubject());
    messageArea.setText(mail.getText());

    String dests = mail.getDestList().toString();
    // To remove the brackets ( [ ..., ... ]
    destField.setText(dests.subSequence(1, dests.length() - 1).toString());

    openedMail = mail;
  }

  public void newMail() {
    sendButton.setVisible(true);
    replyAllButton.setVisible(false);
    replyButton.setVisible(false);
    forwardButton.setVisible(false);

    destLabel.setText("To:");

    messageArea.setEditable(true);
    subjectField.setEditable(true);
    destField.setEditable(true);
  }

  @FXML
  public void onCancel() {
    exit();
  }

  private void exit() {
    Stage stage = (Stage) sendButton.getScene().getWindow();
    stage.close();
  }

  @FXML
  public void onSend() {
    try {
      ArrayList<User> destList = new ArrayList<>();
      for (String user: destField.getText().split(",")) {
        destList.add(new User(user.trim()));
        System.out.println(user.trim());
      }

      String message = messageArea.getText();
      if (onReply) {
        message += "\n\nOn " + openedMail.getTimestamp() + " ";
        message += openedMail.getSender() + " wrote:\n";
        message += "> " + openedMail.getText().replaceAll("(\r\n|\n)", "$1" + "> ").replaceAll("> >", ">>");
      }

      clientManager.sendMail(destList, subjectField.getText(), message);
      exit();
    }
    catch (ProtocolException pe) {
      if (pe.getError().equals(Error.DEST_NOT_FOUND)) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Inexistent user");
        alert.setHeaderText("Impossible send a message to a inexistent address.");
        alert.getDialogPane().setContent(new Text(pe.getErrorToken().getMessage()));
        alert.showAndWait();
        if (onReply)
          openMail(openedMail);
      }
      else {
        throw pe;
      }
    }
    catch (ConnectException ce) {
      Platform.runLater( () -> {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Impossible send the mail");
        alert.setHeaderText("Impossible send the mail");
        alert.setContentText("The server may be offline or unreachable.");
        alert.showAndWait();
      });
    }
  }

  @FXML
  public void onReply() {
    newMail();
    subjectField.setEditable(false);
    destField.setEditable(false);

    subjectField.setText("Re: " + openedMail.getSubject());
    destField.setText(openedMail.getSender().toString());
    messageArea.setText("");
    
    onReply = true;
  }

  @FXML
  public void onReplyAll() {
    newMail();
    subjectField.setEditable(false);
    destField.setEditable(false);

    subjectField.setText("Re: " + openedMail.getSubject());

    ArrayList<User> dests = openedMail.getDestList();
    dests.remove(new User(clientManager.getAddressName()));

    if (dests.isEmpty())
      destField.setText(openedMail.getSender().toString());
    else
      destField.setText(openedMail.getSender() + ", " + 
        dests.toString().subSequence(1, dests.toString().length() -1));

    messageArea.setText("");
    
    onReply = true;
  }

  @FXML
  public void onForward() {
    TextInputDialog dialog = new TextInputDialog(clientManager.getAddressName());

    dialog.setTitle("Forward message");
    dialog.setHeaderText("Enter destination addresses separated by commas.");
    dialog.setContentText("Forward to:");

    Optional<String> result = dialog.showAndWait();

    result.ifPresent( dests -> {
      onReply = true;
      subjectField.setText("Fwd: " + openedMail.getSubject());
      destField.setText(dests);
      messageArea.setText("");

      onSend();
    });
  }
}
