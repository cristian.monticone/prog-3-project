package controller;

import server.ServerManager;
import data.DataModel;
import data.Logger;
import data.LogLevel;
import data.Config;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.Menu;
import javafx.scene.layout.BorderPane;

import javafx.application.Platform;

import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.Modality;

import javafx.scene.paint.Color;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

import java.io.IOException;

public class ServerController {
    ServerManager serverManager;
    Logger logger;

    @FXML
    private BorderPane mainPane;

    @FXML
    private Menu configMenu;

    @FXML
    private TextArea logArea;

    @FXML
    private Label serverStatus;

    @FXML
    public void initialize() {
      serverManager = new ServerManager();
      logger = serverManager.getModel().getLogger();

      logger.addReceiver( (String msg) -> {
        Platform.runLater( () -> {
          String logText = logArea.getText();
          logArea.setText(logText + "\n" + msg);

          // Autoscroll
          logArea.selectPositionCaret(logArea.getLength());
          logArea.deselect(); //removes the highlighting
        });
      });

      serverStatus.setText("OFFLINE");
      serverStatus.setTextFill(Color.RED);
    }

    @FXML
    public void onServerStart() {
      configMenu.setDisable(true);
      serverManager.start();
      serverStatus.setText("ONLINE");
      serverStatus.setTextFill(Color.GREEN);
    }

    @FXML
    public void onServerStop() {
      serverManager.stop();
      configMenu.setDisable(false);
      serverStatus.setText("OFFLINE");
      serverStatus.setTextFill(Color.RED);
    }

    @FXML
    public void onOpenConfig() {
      FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/config.fxml"));
      
      Parent parent = null;

      try {
          parent = fxmlLoader.load();
          fxmlLoader.<ConfigController>getController().setModel(serverManager.getModel());
          fxmlLoader.<ConfigController>getController().setLogWriter(serverManager.getLogWriter());
          Scene scene = new Scene(parent);
          Stage stage = new Stage();
          stage.initModality(Modality.APPLICATION_MODAL);
          stage.setResizable(false);
          stage.setTitle("Server configuration");
          stage.setScene(scene);
          stage.showAndWait();
      } catch (IOException ex) {
          ex.printStackTrace();
      }
    }

    @FXML
    public void onResetConfig() {
      Config config = serverManager.getModel().getConfig();
      config.loadDefault();
      config.writeConfig();
      logger.log(LogLevel.SYSTEM, "Server configuration setted to the default" +
                                  " values.");
    }

    @FXML
    public void onAbout() {
      Alert alert = new Alert(AlertType.INFORMATION);
      alert.setTitle("About");
      alert.setHeaderText("Prog3 Mail Server");
      alert.setContentText("Written by Cristian Monticone and Giordano Scarso.");
      alert.showAndWait();
    }
}
