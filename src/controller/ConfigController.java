package controller;

import javafx.fxml.FXML;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.stage.Stage;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

import server.ServerManager;
import data.Logger;
import data.LogLevel;
import data.LogWriter;
import data.DataModel;
import data.Config;

public class ConfigController {
  private DataModel model;
  private Config config;
  private Logger logger;
  private LogWriter logWriter;

  @FXML
  private Button saveButton;

  @FXML
  private TextField address;

  @FXML
  private TextField port;

  @FXML
  private TextField logPath;

  @FXML
  private ChoiceBox<LogLevel> logLevelChoiceBox;

  public void initialize() {
    port.textProperty().addListener(new ChangeListener<String>() {
        @Override
        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
          if (!newValue.equals(""))
            if (!newValue.matches("\\d{0,5}?") || Integer.parseInt(newValue) > 65536)
                port.setText(oldValue);

          saveButton.setDisable(newValue.equals("") || !allSetted());
        }
    });

    address.textProperty().addListener(new ChangeListener<String>() {
        @Override
        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
          saveButton.setDisable(newValue.equals("") || !allSetted());
        }
    });

    logPath.textProperty().addListener(new ChangeListener<String>() {
        @Override
        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
          saveButton.setDisable(newValue.equals("") || !allSetted());
        }
    });

    ObservableList<LogLevel> data = FXCollections.observableArrayList(LogLevel.getAllLevels());
    logLevelChoiceBox.setItems(data);
  }

  @FXML
  public void onExit() {
   Stage stage = (Stage) port.getScene().getWindow();
   stage.close(); 
  }

  @FXML
  public void onSave() {
    logger.log(LogLevel.SYSTEM, "Server configuration changed.");

    config.setPort(Integer.parseInt(port.getText()));
    config.setListeningAddress(address.getText());
    config.setLogPath(logPath.getText());
    config.setLogLevel(logLevelChoiceBox.getValue());
    model.getLogger().changeLogLevel(logLevelChoiceBox.getValue());

    config.writeConfig();
    logWriter.updatePath();

    onExit();
  }

  public void setModel(DataModel model) {
    this.model = model;
    this.config = model.getConfig();
    this.logger = model.getLogger();

    address.setText(config.getListeningAddress());
    port.setText(Integer.toString(config.getPort()));
    logPath.setText(config.getLogPath());
    logLevelChoiceBox.setValue(config.getLogLevel());
  }

  public void setLogWriter(LogWriter logWriter) {
    this.logWriter = logWriter;
  }

  private boolean allSetted() {
    return !port.getText().equals("") && 
           !address.getText().equals("") &&
           !logPath.getText().equals("");
  }
}
