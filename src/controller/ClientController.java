package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.Label;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextInputDialog;
import javafx.scene.text.Text;
import javafx.application.Platform;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.stage.Modality;

import java.io.IOException;
import java.net.ConnectException;

import client.ClientManager;
import data.Mail;
import data.User;
import data.Error;

import protocol.ProtocolException;
import protocol.ErrorToken;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import java.lang.Thread;

public class ClientController {
    private ClientManager clientManager;
    private volatile boolean running;
    private volatile boolean disableNextAlert = true;
    public final static int pollingRate = 1000; // 1000ms = 1s
    private Thread mailUpdater;
    private Mail selectedMail;

    @FXML
    private TableView<Mail> mailListView;

    @FXML
    private TableColumn<Mail, String> senderCol;

    @FXML
    private TableColumn<Mail, String> subjectCol;

    @FXML
    private TableColumn<Mail, Date> dateCol;

    @FXML
    private Button removeButton;

    @FXML
    private Button openButton;

    @FXML
    private Button sendButton;

    @FXML
    public void initialize() {
      clientManager = new ClientManager();

      mailListView.setPlaceholder(new Label("Attempting to retrieve your messages..."));

      List<Mail> mails = new ArrayList<>();
      ObservableList<Mail> data = FXCollections.observableArrayList(mails);
      mailListView.setItems(data);

      senderCol.setCellValueFactory(new PropertyValueFactory<Mail, String>("sender"));
      subjectCol.setCellValueFactory(new PropertyValueFactory<Mail, String>("subject"));
      dateCol.setCellValueFactory(new PropertyValueFactory<Mail, Date>("timestamp"));
        
      mailListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

      mailListView.getSelectionModel().selectedIndexProperty().addListener((observable) -> {
          selectedMail = mailListView.getSelectionModel().getSelectedItem();
          removeButton.setDisable(false);
          openButton.setDisable(false);
      });

      mailListView.setOnMouseClicked( (clickEvent) -> {
        if (clickEvent.getClickCount() == 2 && selectedMail != null) {
          onOpen();
        }
      });

      running = true;

      mailUpdater = new Thread( () -> {
        boolean conError = false;
        try {
          while (running) {
            Thread.sleep(1000); // Polling rate: 1 second

            List<Mail> newMails = new ArrayList<>();
            try {
              newMails = clientManager.synchMails();

              Platform.runLater( () -> {
                mailListView.setPlaceholder(new Label("No messages in your inbox."));
                sendButton.setDisable(false);
              });

              conError = false;
            }
            catch (ConnectException ce) {
              //System.out.println("[ERROR] Mail synch request failed: Server unreachable.");
              // TODO a logger in the client.
              conError = true;
              disableNextAlert = true;

              Platform.runLater( () -> {
                mailListView.setPlaceholder(new Label("Server unreachable! Trying to reconnect..."));
                resetInbox();
                sendButton.setDisable(true);
                removeButton.setDisable(true);
                openButton.setDisable(true);
              });
            }
            catch (ProtocolException pe) {
              if (pe.getError().equals(Error.DEST_NOT_FOUND)) {
                Platform.runLater( () -> {
                  changeAddress("default@prog3mail.com");
                  Alert alert = new Alert(AlertType.ERROR);
                  alert.setTitle("Inexistent user");
                  alert.setHeaderText("Impossible found this user address");
                  alert.getDialogPane().setContent(new Text("Username setted" +
                            " to the default address default@prog3mail.com"));
                  alert.showAndWait();
                });
              }
              else {
                throw pe;
              }
            }

            mailListView.getItems().addAll(newMails);
            
            if (!newMails.isEmpty() && !disableNextAlert) {
              Platform.runLater( () -> {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("New mail received");
                alert.setHeaderText("New mail received");
                alert.setContentText("Check your inbox for the new mails received.");
                alert.showAndWait();
              });
            }

            if (!conError) disableNextAlert = false;
          }
        }
        catch (InterruptedException ie) { System.out.println("Mail updater stopped."); }
        catch (Exception e) { e.printStackTrace(); }
      });

      mailUpdater.start();
    }

    private void resetInbox() {
      mailListView.getItems().clear();
      clientManager.resetLastMail();
    }

    @FXML
    public void onSend() {
      FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/mailform.fxml"));
      
      Parent parent = null;

      try {
          parent = fxmlLoader.load();
          fxmlLoader.<MailFormController>getController().setClientManager(clientManager);
          fxmlLoader.<MailFormController>getController().newMail();
          Scene scene = new Scene(parent);
          Stage stage = new Stage();
          stage.initModality(Modality.APPLICATION_MODAL);
          stage.setResizable(false);
          stage.setTitle("Send new mail");
          stage.setScene(scene);
          stage.showAndWait();
      } catch (IOException ex) {
          ex.printStackTrace();
      }
    }

    @FXML
    public void onOpen() {
      FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../view/mailform.fxml"));
      
      Parent parent = null;

      try {
          parent = fxmlLoader.load();
          fxmlLoader.<MailFormController>getController().setClientManager(clientManager);
          fxmlLoader.<MailFormController>getController().openMail(selectedMail);
          Scene scene = new Scene(parent);
          Stage stage = new Stage();
          stage.initModality(Modality.APPLICATION_MODAL);
          stage.setResizable(false);
          stage.setTitle(selectedMail.getSubject());
          stage.setScene(scene);
          stage.showAndWait();
      } catch (IOException ex) {
          ex.printStackTrace();
      }
    }

    @FXML
    public void onRemove() {
      try {
        clientManager.removeMail(selectedMail);
      }
      catch (ConnectException ce) {
        Platform.runLater( () -> {
          Alert alert = new Alert(AlertType.ERROR);
          alert.setTitle("Impossible remove this mail");
          alert.setHeaderText("Impossible remove this mail");
          alert.setContentText("The server may be offline or unreachable.");
          alert.showAndWait();
        });
      }
      mailListView.getItems().remove(selectedMail);
      
      if(mailListView.getItems().isEmpty()) {
        removeButton.setDisable(true);
        openButton.setDisable(true);
      }
      else {
        selectedMail = mailListView.getSelectionModel().getSelectedItem();
      }
    }

    @FXML
    public void onClose() {
      running = false;
      mailUpdater.interrupt();
      System.out.println("Mail client closed.");
      Platform.exit();
    }

    @FXML
    public void onChangeUser() {
      TextInputDialog dialog = new TextInputDialog(clientManager.getAddressName());

      dialog.setTitle("Change user");
      dialog.setHeaderText("Enter your new mail address:");
      dialog.setContentText("Mail address:");

      Optional<String> result = dialog.showAndWait();

      result.ifPresent( name -> changeAddress(name) );
    }

    private void changeAddress(String address) {
      clientManager.changeUser(new User(address));

      setTitle(address);

      mailListView.setPlaceholder(new Label("Retriving messages for the new user..."));
      resetInbox();
      disableNextAlert = true;
    }

    @FXML
    public void onAbout() {
      Alert alert = new Alert(AlertType.INFORMATION);
      alert.setTitle("About");
      alert.setHeaderText("Prog3 Mail Client");
      alert.setContentText("Written by Cristian Monticone and Giordano Scarso.");
      alert.showAndWait();
    }

    private void setTitle(String title) {
      Stage stage = (Stage)sendButton.getScene().getWindow();
      stage.setTitle(title);
    }
}
