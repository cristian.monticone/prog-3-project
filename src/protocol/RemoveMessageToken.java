package protocol;

/**
 * A token representing a request of mail removing.
 * <p>A {@link AckToken} or {@link ErrorToken} is sent back based on the success
 * or failure of the request.</p>
 *
 * @see ProtocolToken
 * @see RequestToken
 */
public class RemoveMessageToken extends RequestToken {
  private Long id;

  /**
   * The RemoveMessageToken constructor.
   *
   * @param id The univocal long identifier of the mail to be removed.
   */
  public RemoveMessageToken(Long id) {
    this.id = id;
  }

  /**
   * Gets the univocal long identifier of the mail to be removed.
   *
   * @return A univocal long identifier of the mail to be removed.
   */
  public Long getId() { return id; }

  /**
   * Sets the univocal long identifier of the mail to be removed.
   *
   * @param id A univocal long identifier of the mail to be removed.
   */
  public void setId(Long id) { this.id = id; }
}
