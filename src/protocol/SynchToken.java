package protocol;

/**
 * A token representing a request of synchronization, a {@link MessagesToken}
 * is expected back as response.
 *
 * @see ProtocolToken
 * @see RequestToken
 */ 
public class SynchToken extends RequestToken {
  private Long id;

  /**
   * Constructor for class SynchToken.
   *
   * @param id The univocal long identifier of the latest received mail.
   */
  public SynchToken(Long id) {
    this.id = id;
  }

  /**
   * Gets the univocal long identifier of the latest client's received mail.
   *
   * @return A univocal long identifier of the latest client's received mail.
   */
  public Long getId() { return id; }

  /**
   * Sets the univocal long identifier of the latest client's received mail.
   *
   * @param id The univocal long identifier of the latest client's received
   * mail.
   */
  public void setId(Long id) { this.id = id; }
}
