package protocol;

import java.net.Socket;
import java.io.IOException;
import java.util.List;

import data.User;
import data.Mail;
import data.Error;

/**
 * A class designed to handle the protocol networking from client to the server.
 */
public class ProtocolClient extends ProtocolHandler {
  private boolean initialized = false;

  /**
   * The class constructor.
   *
   * @param s A connected socket to the server.
   */
  public ProtocolClient(Socket s) throws IOException {
    super(s);
  }

  /**
   * Initializes the connection with a user.
   * <p>It can also fail if user not exist or is invalid.</p>
   *
   * @param u Who wants to establish the connection.
   */
  public void init(User u) throws IOException {
    // Init handshake
    InitToken t = new InitToken(u);
    sendToken(t);

    // Wait response
    ResponseToken r;
    try {
      r = (ResponseToken) receiveToken();
    }
    catch (ClassCastException cce) {
      throw new ProtocolException("Received a non ResponseToken object from " +
                                  "server.", Error.INVALID_DATA);
    }

    if (r.getClass() == AckToken.class) {
      initialized = true;
    }
    else if (r.getClass() == ErrorToken.class) {
      ErrorToken err = (ErrorToken) r;
      throw new ProtocolException("Error token received from server " +
                                  "initializing the connection", err);
    }
  }

  /**
   * Performs a request of mail synchronization.
   * <p>All mail in the user mailbox will be sent back.</p>
   */
  public List<Mail> synchRequest() throws IOException {
    return synchRequest(null);
  }

  /**
   * Performs a request of mail synchronization.
   * <p>All mail with identifier bigger than parameter id will be sent
   * back.</p>
   *
   * @param id An univoc long identifier of the last user's mail.
   */
  public List<Mail> synchRequest(Long id) throws IOException {
    checkInitialization();

    sendToken(new SynchToken(id));

    MessagesToken msgs;
    try {
      msgs = (MessagesToken) receiveToken();
    }
    catch (ClassCastException cce) {
      throw new ProtocolException("Received a non MessagesToken object from " +
                                  "server.", Error.INVALID_DATA);
    }

    return msgs.getMailList();
  }

  /**
   * Sends a mail send request to the server.
   * <p>The server can respond with an {@link ErrorToken}.</p>
   *
   * @param mail The mail to send.
   */
  public void sendMessageRequest(Mail mail) throws IOException {
    checkInitialization();

    if (mail == null)
      throw new NullPointerException("Trying to send a mail with null value.");

    // TODO other checks 
    
    sendToken(new SendMessageToken(mail));

    // Wait server response
    ResponseToken response;
    try {
      response = (ResponseToken) receiveToken();
    }
    catch (ClassCastException cce) {
      throw new ProtocolException("Received a non ResponseToken object from " +
                                  "server.", Error.INVALID_DATA);
    }
    
    if (response.getClass() == AckToken.class) {
      System.out.println("Server: message sent properly.");
    }
    else if (response.getClass() == ErrorToken.class) {
      ErrorToken err = (ErrorToken) response;
      throw new ProtocolException("Server response with an error token " +
                                  "sending a mail.", err);
    }
  }

  /**
   * Sends a mail remove request to the server.
   * <p>The server can respond with an {@link ErrorToken}.</p>
   *
   * @param mail The mail to remove.
   */
  public void sendRemoveMessageRequest(Long id) throws IOException {
    checkInitialization();

    sendToken(new RemoveMessageToken(id));

    // Wait server response
    ResponseToken response;
    try {
      response = (ResponseToken) receiveToken();
    }
    catch (ClassCastException cce) {
      throw new ProtocolException("Received a non ResponseToken object from " +
                                  "server.", Error.INVALID_DATA);
    }
    
    if (response.getClass() == AckToken.class) {
      System.out.println("Server: message removed properly.");
    }
    else if (response.getClass() == ErrorToken.class) {
      ErrorToken err = (ErrorToken) response;
      throw new ProtocolException("Server response with an error token " +
                              "removing a mail.", err);
    }
  }

  private void checkInitialization() {
    if (!initialized) {
      throw new ProtocolException("Trying to send a request without a " +
                                  "properly instantiated connection.",
                                  Error.BAD_REQUEST);
    }
  }
}

