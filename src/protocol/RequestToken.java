package protocol;

/**
 * A token representing requests that should be sent to the server.
 * <p>A {@link ResponseToken} is expected as response after this token.</p>
 *
 * @see InitToken
 * @see SynchToken
 * @see SendMessageToken
 * @see RemoveMessageToken
 */
public abstract class RequestToken extends ProtocolToken {
  
  /**
   * A fictitious constructor.
   */
  public RequestToken() {}
}
