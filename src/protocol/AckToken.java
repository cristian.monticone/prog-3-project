package protocol;

/**
 * A token class representing a success sent back after a {@link RequestToken}.
 *
 * @see ProtocolToken
 * @see ResponseToken
 * @see ErrorToken
 */
public class AckToken extends ResponseToken {
  /**
   * A fictitious constructor.
   */
  public AckToken() {
  }
}
