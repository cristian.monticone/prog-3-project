package protocol;

/**
 * A token that should be sent after a {@link RequestToken}.
 *
 * @see MessagesToken
 * @see AckToken
 * @see ErrorToken
 */
public abstract class ResponseToken extends ProtocolToken {
  
  /**
   * A fictitious constructor.
   */
  public ResponseToken() {}
}
