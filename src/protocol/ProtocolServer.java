package protocol;

import data.User;
import data.Mail;
import data.DataModel;
import data.Logger;
import data.LogLevel;
import data.Error;

import java.util.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.net.Socket;
import java.io.IOException;

public class ProtocolServer extends ProtocolHandler {
  private DataModel model;
  private User clientUser;
  private Logger logger;

  public ProtocolServer(Socket s, DataModel model) throws IOException {
    super(s);

    this.model = model;
    logger = model.getLogger();
  }

  public void handle() throws IOException {
    InitToken initToken;

    // Waiting for init token from client
    try {
      initToken = (InitToken) receiveToken();
    }
    catch (ClassCastException cce) {
      logger.log(LogLevel.WARN,
                 "Illegal connection established from unknown client.");
      throw new ProtocolException("Received a non InitToken object from " +
                                  "client.", Error.BAD_REQUEST);
    }

    clientUser = initToken.getUser();

    // User check and response
    if (model.userExists(clientUser)) { // Connected user exist?
      sendToken(new AckToken());
      logger.log(LogLevel.CONN, "The user " +  clientUser +
                 " had established a connection.");
    }
    else {
      logger.log(LogLevel.WARN, "The inexistent user " + clientUser +
                 " tried to connect.");

      ErrorToken error = new ErrorToken(Error.DEST_NOT_FOUND, 
                                        "Address " + clientUser.getAddress() +
                                             " not found.");
      sendToken(error);
      throw new ProtocolException("User " + clientUser.getAddress() + 
                    " don't exist.", Error.DEST_NOT_FOUND);
    }

    // Handle client request
    RequestToken request;
    try {
      request = (RequestToken) receiveToken();
    }
    catch (ClassCastException cce) {
      logger.log(LogLevel.ERROR, "Received a corrupted token.");
      throw new ProtocolException("Received a non RequestToken object from " +
                                  "client.", Error.BAD_REQUEST);
    }

    handleRequest(request);

    logger.log(LogLevel.CONN, "Correctly closed connection from " +
               clientUser + ".");
  }

  private void handleRequest(RequestToken t) throws IOException {
    Class tokenClass = t.getClass();

    if(tokenClass == SynchToken.class)
      handleSynchRequest((SynchToken)t);
    else if(tokenClass == RemoveMessageToken.class)
      handleRemoveMessageToken(t);
    else if(tokenClass == SendMessageToken.class)
      handleSendMessageToken(t);
    else
      throw new ProtocolException(t.getClass().getName() + " request not " +
                          "implemented.", Error.NOT_IMPLEMENTED);
  }

  private void handleSynchRequest(SynchToken t) throws IOException {
    logger.log(LogLevel.CONN, 
               "Synchronization request from " + clientUser + ".");

    Long lastId = t.getId();

    ArrayList<Mail> mailList = model.getMails(clientUser, lastId);

    MessagesToken msgs = new MessagesToken(mailList);

    logger.log(LogLevel.DEBUG,
               "Server response to " + clientUser + " with: " + mailList);

    sendToken(msgs);
  }

  private void handleRemoveMessageToken(RequestToken t) throws IOException {
    RemoveMessageToken request;
    try {
      request = (RemoveMessageToken) t;
    }
    catch (ClassCastException cce) {
      logger.log(LogLevel.ERROR, "Received a corrupted token.");
      throw new ProtocolException("Received a non RemoveMessageToken object " +
                                  "from client.", Error.BAD_REQUEST);
    }

    logger.log(LogLevel.INFO, "Remove message [" + request.getId() + "] request from " + clientUser + ".");

    model.removeMail(clientUser, request.getId());

    sendToken(new AckToken());
  }

  private void handleSendMessageToken(RequestToken t) throws IOException {
    SendMessageToken request;
    try {
      request = (SendMessageToken) t;
    }
    catch (ClassCastException cce) {
      logger.log(LogLevel.ERROR, "Received a corrupted token.");
      throw new ProtocolException("Received a non SendMessageToken object " +
                                  "from client.", Error.BAD_REQUEST);
    }

    Mail m = request.getMail();

    m.setSender(clientUser);
    m.setTimestamp(new Date());

    if (m.getSubject() == null) m.setSubject("[No subject]");
    if (m.getText() == null) m.setText("[No inner text]");

    logger.log(LogLevel.INFO, clientUser + " sent a mail to " + m.getDestList());
    logger.log(LogLevel.DEBUG, "sent mail: " + m);

    ArrayList<User> inexistents = new ArrayList<>();
    for (User dest: m.getDestList()) {
      if (!model.userExists(dest))
        inexistents.add(dest);
    }

    if (!inexistents.isEmpty()) {
      logger.log(LogLevel.WARN, clientUser + " tried to send and email to " +
                 "this inexistent users: " + inexistents);

      sendToken(new ErrorToken(Error.DEST_NOT_FOUND, 
                               "Trying to send a mail to this inexistent" +
                               " users: " + inexistents));
    }
    else {
      model.sendMail(m);
      sendToken(new AckToken());
    }
  }
}
