package protocol;

import data.User;

/**
 * A initialization token, it must be the first token sent by clients.
 * <p>It allows the server to identify the user and check his existence.</p>
 * <p>A {@link AckToken} or {@link ErrorToken} is sent back based on the success
 * or failure of the request.</p>
 *
 * @see ProtocolToken
 * @see RequestToken
 */
public class InitToken extends RequestToken {
  private User u;

  /**
   * Constructor for class InitToken.
   *
   * @param u A {@link User} representing who wants perform the initialization.
   */
  public InitToken(User u) {
    this.u = u;
  }

  /**
   * Gets the user who wants perform the initialization.
   * 
   * @return A {@link User} representing who wants perform the initialization.
   */
  public User getUser() { return this.u; }

  /**
   * Sets the user who wants perform the initialization.
   *
   * @param u A {@link User} representing who wants perform the initialization.
   */
  public void setUser(User u) { this.u = u; }
}
