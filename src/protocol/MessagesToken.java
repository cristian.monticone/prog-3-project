package protocol;

import java.util.List;
import data.Mail;

/**
 * A token class representing responses of {@link SynchToken}.
 * <p>It contains a set of mails sent back to the client after a Synch 
 * request.</p>
 */
public class MessagesToken extends ResponseToken {
  private List<Mail> mailList;

  /**
   * The MessagesToken contains.
   *
   * @param mailList An ArrayList of mail to be sent to the clients.
   */
  public MessagesToken(List<Mail> mailList) {
    setMailList(mailList);
  }

  /**
   * Gets the mails to be sent to the clients.
   *
   * @return An ArrayList of mails to be sent to the clients.
   */
  public List<Mail> getMailList() { return mailList; }

  /**
   * Sets the mails to be sent to the clients.
   *
   * @param mailList An ArrayList of mails to be sent to the clients.
   */
  public void setMailList(List<Mail> mailList) {
    this.mailList = mailList;
  }
}
