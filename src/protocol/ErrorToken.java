package protocol;

import data.Error;

/**
 * A token class representing a failure sent back after a {@link RequestToken}.
 *
 * @see Error
 * @see ProtocolToken
 * @see ResponseToken
 * @see AckToken
 */
public class ErrorToken extends ResponseToken {
  private String message;
  private Error error;

  /**
   * The ErrorToken constructor.
   *
   * @param error An Error object.
   * @param message A error message.
   */
  public ErrorToken(Error error, String message) {
    setError(error);
    setMessage(message);
  }

  /**
   * Gets the token error number.
   *
   * @return An Error object.
   */
  public Error getError() { return error; }

  /**
   * Gets the token error message.
   *
   * @return An error message.
   */
  public String getMessage() { return message; }

  /**
   * Sets the token error number.
   *
   * @param error An Error object.
   */
  public void setError(Error error) {
    this.error = error;
  }

  /**
   * Sets the token error message.
   *
   * @param message A error message.
   */
  public void setMessage(String message) { this.message = message; }

  @Override
  public String toString() { return "error=" + error + 
                                    ", message=\"" + message + "\""; }
}
