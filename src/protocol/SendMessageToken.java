package protocol;

import data.Mail;

/**
 * A token carrying a mail that should be delivered by the server.
 * <p>A {@link AckToken} or {@link ErrorToken} is sent back based on the success
 * or failure of the request.</p>
 *
 * @see ProtocolToken
 * @see RequestToken
 */
public class SendMessageToken extends RequestToken {
  private Mail mail;

  /**
   * The SendMessageToken constructor.
   *
   * @param mail The mail to be sent.
   */
  public SendMessageToken(Mail mail) {
    this.mail = mail;
  }

  /**
   * Sets the mail to be sent.
   *
   * @param mail The mail to be sent.
   */
  public void setMail(Mail mail) { this.mail = mail; }

  /**
   * Gets the mail to be sent.
   *
   * @return The mail to be sent.
   */
  public Mail getMail() { return mail; }
}
