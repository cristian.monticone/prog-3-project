package protocol;

import data.Error;

public class ProtocolException extends RuntimeException {
  private ErrorToken errorToken;

  public ProtocolException(String msg, ErrorToken token) {
      super(msg);
      this.errorToken = token;
  }

  public ProtocolException(String msg) {
    super(msg);
    this.errorToken = new ErrorToken(Error.UNKNOWN, 
                                     "Error token not provided.");
  }

  public ProtocolException(String msg, Error error) {
    super(msg);
    this.errorToken = new ErrorToken(error, msg);
  }

  public ErrorToken getErrorToken() {
    return errorToken; 
  }

  public int getStatus() {
    return errorToken.getError().getCode();
  }

  public Error getError() {
    return errorToken.getError();
  }

  @Override
  public String toString() {
    return "ExceptionMessage: " + super.getMessage() + 
           ", ErrorTokenCode: " + getStatus() +
           ", ErrorName: " + getError() +
           ", ErrorTokenMessage: " + errorToken.getMessage();
  }
}
