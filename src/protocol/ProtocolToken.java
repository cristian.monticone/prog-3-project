package protocol;

import java.io.Serializable;

/**
 * An abstract class representing a protocol token.
 * <p>Protocol tokens are sent between server and client.</p>
 * <p>This class is serializable because it must be transmitted over
 * sockets.</p>
 *
 * @see RequestToken
 * @see ResponseToken
 */
public abstract class ProtocolToken implements Serializable {

  /**
   * A fictitious constructor.
   */
  public ProtocolToken() {}
}
