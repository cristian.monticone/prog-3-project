package protocol;

import data.Error;

import java.net.Socket;
import java.io.IOException;
import java.io.EOFException;
import java.io.StreamCorruptedException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.ClassCastException;
import java.lang.IllegalArgumentException;

/**
 * This class provide an handler able to receive and send token objects
 * throught a socket.
 *
 * @see ProtocolToken
 */
abstract public class ProtocolHandler {
  private ObjectOutputStream outStream;
  private ObjectInputStream inStream;
  private Socket s;
  
  /**
   * The class constructor.
   *
   * @param s A connected socket where protocol tokens will be sent and 
   * received.
   */
  public ProtocolHandler(Socket s) throws IOException {
    if (!s.isConnected())
      throw new IllegalArgumentException("Trying to handle a not connected " +
                                         "socket.");

    this.s = s;
    this.outStream = new ObjectOutputStream(s.getOutputStream());
    try {
      this.inStream = new ObjectInputStream(s.getInputStream());
    }
    catch (StreamCorruptedException sce) {
      throw new ProtocolException("Corrupted input from client.");
    }
  }

  public void closeConnection() throws IOException {
    s.close();
  }

  protected void sendToken(ProtocolToken t) throws IOException {
    outStream.writeObject(t);
  }

  protected ProtocolToken receiveToken() throws IOException {
    ProtocolToken t = null;

    try {
      t = (ProtocolToken) inStream.readObject();
    }
    catch (ClassCastException cce) {
      throw new ProtocolException("Received a non ProtocolToken object from " +
                                  "client.", Error.INTERNAL);
    }
    catch (ClassNotFoundException e) {
      throw new ProtocolException("Class definition of a serialized object " +
                                  "cannot be found.",
                                  Error.INTERNAL);
    }
    catch (StreamCorruptedException sce) {
      throw new ProtocolException("Corrupted input from client.", 
                                  Error.INTERNAL);
    }
    catch (EOFException eofe) {
      throw new ProtocolException("The client has closed the connection.",
                                  Error.INTERNAL);
    }

    return t;
  }
}
